from sqlalchemy.orm import Session
from entities.news import News
from schemas import newsSchema

def get_all(db: Session):
    return db.query(News).order_by(News.date.desc()).all()

def create(db: Session, news: newsSchema.NewsBase):
    db_news = News(news.title, news.content)
    db.add(db_news)
    db.commit()
    db.refresh(db_news)
    return db_news

def update_news(db: Session, news: newsSchema.NewsUpdate):
    db_news_session = db.query(News).filter(News.id == news.id)
    db_news_session.update(news)
    db.commit()

    db_news = db_news_session.one()
    db.refresh(db_news)
    return db_news

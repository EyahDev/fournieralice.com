from sqlalchemy.orm import Session
from entities.image import Image
from schemas import imageSchema

def get_all(db: Session):
    return db.query(Image).all()

def create_all(db: Session, images):
    db.bulk_insert_mappings(Image, images)
    db.commit()
    return images

from sqlalchemy.orm import Session
from entities.user import User
from schemas import userSchema

def get_user(db: Session, user_id: int):
    return db.query(User).filter(User.id == user_id).first()

def get_user_by_email(db: Session, email: str):
    return db.query(User).filter(User.email == email).first()

def create_user(db: Session, user: userSchema.UserCreate, hashed_password: str):
    db_user = User(user.email, hashed_password, user.firstname, user.lastname, user.phone)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user

def update_user(db: Session, user: userSchema.User):
    db_user_session = db.query(User).filter(User.id == user.id)
    db_user_session.update(user)
    db.commit()

    db_user = db_user_session.one()
    db.refresh(db_user)
    delattr(db_user, 'password')
    return db_user

def update_user_password(db: Session, user: userSchema.User, hashed_new_password: str):
    db_user = db.query(User).filter(User.id == user.id).first()
    db_user.password = hashed_new_password
    db.commit()
    db.refresh(db_user)
    return True

def get_about_section(db: Session):
    return db.query(User.about).filter(User.about != None).first()

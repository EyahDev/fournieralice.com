# coding=utf-8

from sqlalchemy import Column, String, Integer, UnicodeText
from database import Base

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    password = Column(String)
    firstname = Column(String)
    lastname = Column(String)
    phone = Column(String)
    about = Column(UnicodeText, nullable=True)

    def __init__(self, email, password, firstname, lastname, phone):
        self.email = email
        self.password = password
        self.firstname = firstname
        self.lastname = lastname
        self.phone = phone

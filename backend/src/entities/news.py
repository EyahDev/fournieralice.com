# coding=utf-8

from sqlalchemy import Column, Integer, String, UnicodeText, DateTime, sql
from database import Base

class News(Base):
    __tablename__ = 'news'

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, nullable=False)
    content = Column(UnicodeText, nullable=False)
    date = Column(DateTime, server_default=sql.func.now())

    def __init__(self, title, content):
        self.title = title
        self.content = content

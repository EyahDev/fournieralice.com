# coding=utf-8

from sqlalchemy import Column, Integer, String, DateTime
from database import Base

class Image(Base):
    __tablename__ = 'images'

    id = Column(Integer, primary_key=True, index=True)
    path = Column(String)
    filename = Column(String, unique=True, index=True)
    description = Column(String)

    def __init__(self, path, filename, description):
        self.path = path
        self.filename = filename
        self.description = description

from fastapi import Depends, Body, APIRouter, HTTPException, Request, Response, status
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from jose import JWTError, jwt

#local import
from api import userApi
from services import authService
from schemas import userSchema
from database import get_db

router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@router.put("/informations")
async def edit_informations(
    user: userSchema.User,
    db: Session = Depends(get_db),
    token: str = Depends(oauth2_scheme),
    userToken: userSchema.UserToken = Depends(authService.get_token_from_header)
):
    if userToken.email != user.email:
        userWithEmail = userApi.get_user_by_email(db, user.email)
        if userWithEmail:
            raise HTTPException(
                status_code=400,
                detail="Cette adresse mail n'est pas disponible",
            )
    return userApi.update_user(db, user)


@router.put("/password")
async def edit_password(
    userData: userSchema.UserPwdReset,
    db: Session = Depends(get_db),
    token: str = Depends(oauth2_scheme)
):
    user = authService.authenticate_user(db, userData.email, userData.password)
    if not user:
        raise HTTPException(
            status_code=400,
            detail="Le mot de passe saisie est incorrecte",
        )

    hashed_new_password = authService.hash_password(userData.new_password)
    if not authService.verify_password(userData.confirmation, hashed_new_password):
        raise HTTPException(
            status_code=400,
            detail="Votre nouveau mot de passe ne correspond pas à la confirmation"
        )
    return userApi.update_user_password(db, user, hashed_new_password)

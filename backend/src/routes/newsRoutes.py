import toml
import time
import os

from fastapi import Depends, APIRouter, HTTPException
from fastapi import FastAPI, File, UploadFile
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from typing import List

#local import
from api import newsApi
from schemas import newsSchema
from database import get_db
from services import fileService, authService

router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
images_config = toml.load('./config/config.toml')['images']

@router.post("")
async def create_single_news(
    news: newsSchema.NewsBase,
    db: Session = Depends(get_db),
    token: str = Depends(oauth2_scheme)
):
    db_news = newsApi.create(db, news)
    if not db_news:
        raise HTTPException(
            status_code=500,
            detail="Une erreur est survenue lors de l'enregistrement",
        )
    return db_news

@router.put("")
async def update_single_news(
    news: newsSchema.NewsUpdate,
    db: Session = Depends(get_db),
    token: str = Depends(oauth2_scheme)
):
    db_news = newsApi.update_news(db, news)
    if not db_news:
        raise HTTPException(
            status_code=500,
            detail="Une erreur est survenue lors de la mise à jour",
        )
    return db_news

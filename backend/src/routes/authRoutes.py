import toml
from fastapi import Depends, Body, APIRouter, HTTPException, Request, Response, status
from jose import JWTError, jwt
from sqlalchemy.orm import Session
from datetime import timedelta
from pydantic import UUID4, EmailStr
from fastapi.security import OAuth2PasswordRequestForm, OAuth2PasswordBearer

#local import
from entities.user import User
from api import userApi
from services import authService
from schemas import userSchema
from database import get_db

router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
jwt_config = toml.load('./config/security.toml')['jwtConfig']


@router.post("/login")
async def login(user: userSchema.UserLogin, db: Session = Depends(get_db)):
    user = authService.authenticate_user(db, user.email, user.password)
    if not user:
        raise HTTPException(
            status_code = status.HTTP_401_UNAUTHORIZED,
            detail = "Vos identifiants sont incorrectes.",
            headers = {"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=jwt_config.get('expire_min'))
    access_token = authService.create_access_token(
        data={"sub": user.email}, expires_delta=access_token_expires
    )
    setattr(user, "auth", {'token': access_token, 'token_type':  'bearer'})

    return {"user": user}

@router.post("/register", response_model=userSchema.User)
async def register(user: userSchema.UserCreate, db: Session = Depends(get_db)):
    db_user = userApi.get_user_by_email(db, user.email)
    if db_user:
        raise HTTPException(status_code=400, detail="Email déjà enregistré")
    hashed_password = authService.hash_password(user.password)
    return userApi.create_user(db, user, hashed_password)


@router.post("/forgotten-password")
async def forgotten_password(userToken: userSchema.UserToken, db: Session = Depends(get_db)):
    user = userApi.get_user_by_email(db, userToken.email)
    if not user:
        raise HTTPException(
            status_code=400,
            detail="L'email saisi n'est pas reconnu.",
            headers={"WWW-Authenticate": "Bearer"},
        )
    email_sent = await authService.send_token_email(userToken)
    if email_sent:
        return {"message": "Un email a été envoyé sur votre boite de messagerie"}
    else:
        raise HTTPException(
            status_code=500,
            detail="Une erreur est survenue durant l'envoie du mail"
        )


@router.post("/reinit-password")
async def reinit_password(userData: userSchema.UserPwdReinit, db: Session = Depends(get_db)):
    userToken = authService.get_email_from_token(userData.token)
    user = userApi.get_user_by_email(db, userToken.email)
    hashed_new_password = authService.hash_password(userData.password)
    updated = userApi.update_user_password(db, user, hashed_new_password)
    if updated:
        return {"message": "Votre mot de passe a bien été réinitialisé"}
    else:
        raise HTTPException(
            status_code=500,
            detail="Une erreur est survenue lors de la réinitialisation"
        )

import toml
import time
import os

from fastapi import Depends, APIRouter, HTTPException
from fastapi import FastAPI, File, UploadFile
from fastapi.security import OAuth2PasswordBearer
from sqlalchemy.orm import Session
from typing import List

#local import
from api import imageApi
from schemas import imageSchema
from database import get_db
from services import fileService, authService

router = APIRouter()
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
images_config = toml.load('./config/config.toml')['images']

@router.post("")
async def save_images(
    db: Session = Depends(get_db),
    images: List[UploadFile] = File(...),
    token: str = Depends(oauth2_scheme)
):
    new_images = []
    for image in images:
        imagename_array = os.path.splitext(image.filename)
        image_new_name = str(time.time()) + imagename_array[1]
        written = fileService.write_uploaded_file(images_config.get('root_path'), image_new_name, image)
        if written:
            new_image = imageSchema.ImageBase(path=os.environ.get('IMAGES_ROOT_PATH'), filename=image_new_name, description=imagename_array[0])
            new_images.append(new_image.dict())
        else:
            raise HTTPException(
                status_code=500,
                detail="Une erreur est survenue lors de l'écriture de l'image sur le disque",
            )
    db_images = imageApi.create_all(db, new_images)
    if not db_images:
        raise HTTPException(
            status_code=500,
            detail="Une erreur est survenue lors de l'enregistrement des métadonnées",
        )
    return db_images

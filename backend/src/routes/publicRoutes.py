import time
import os

from fastapi import Depends, APIRouter, HTTPException
from sqlalchemy.orm import Session

#local import
from api import imageApi, userApi, newsApi
from schemas import imageSchema
from database import get_db

router = APIRouter()

@router.get("/images")
async def get_images(db: Session = Depends(get_db)) -> imageSchema.Image:
    db_images = imageApi.get_all(db)
    if not db_images:
        raise HTTPException(
            status_code=500,
            detail="Aucune image n'a pu être récupéré",
        )
    return db_images


@router.get("/about")
async def get_about_section(db: Session = Depends(get_db)) -> str:
    about_section = userApi.get_about_section(db)
    if not about_section:
        about_section = ['']
    return about_section[0]

@router.get("/news")
async def get_news(db: Session = Depends(get_db)) -> str:
    return newsApi.get_all(db)

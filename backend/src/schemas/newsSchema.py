from datetime import datetime
from typing import List, Optional, Union
from pydantic import BaseModel

class NewsBase(BaseModel):
    title: str
    content: str

class NewsUpdate(NewsBase):
    id: int

class News(NewsBase):
    id: int
    date: datetime

    class Config:
        orm_mode = True

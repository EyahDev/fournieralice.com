from typing import List, Optional, Union
from pydantic import BaseModel

Nested = Union[
    None,
    str,
]

class UserBase(BaseModel):
    email: str
    firstname: str
    lastname: str
    phone: str

class UserLogin(BaseModel):
    email: str
    password: str

class UserToken(BaseModel):
    email: str

class UserPwdReinit(BaseModel):
    password: str
    token: str

class UserPwdForgotten(BaseModel):
    email: str
    new_password: str
    confirmation: str

class UserPwdReset(UserPwdForgotten):
    password: str

class UserCreate(UserBase):
    password: str

class User(UserBase):
    id: int
    about: Nested

    class Config:
        orm_mode = True

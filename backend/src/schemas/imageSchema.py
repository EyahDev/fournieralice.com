from typing import List, Optional
from pydantic import BaseModel

class ImageBase(BaseModel):
    path: str
    filename: str
    description: str

class Image(ImageBase):
    id: int

    class Config:
        orm_mode = True

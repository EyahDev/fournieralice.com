import toml
from fastapi import Depends, FastAPI, Request
from routes import authRoutes, userRoutes, imageRoutes, publicRoutes, newsRoutes
from sqlalchemy.orm import Session
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import OAuth2PasswordRequestForm

#local import
from services import authService
from schemas import userSchema
from database import get_db

config = toml.load('./config/config.toml')
app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=config['cors']['origins'],
    allow_methods=config['cors']['methods'],
    allow_headers=config['cors']['headers'],
)

app.include_router(authRoutes.router,
    prefix="/auth",
    tags=["Authentication"]
)

app.include_router(userRoutes.router,
    prefix="/user",
    tags=["User"],
    dependencies=[Depends(authService.get_token_from_header)]
)

app.include_router(imageRoutes.router,
    prefix="/images",
    tags=["Image"],
    dependencies=[Depends(authService.get_token_from_header)]
)

app.include_router(newsRoutes.router,
    prefix="/news",
    tags=["News"],
    dependencies=[Depends(authService.get_token_from_header)]
)

app.include_router(publicRoutes.router,
    tags=["Public"]
)

@app.post("/token", include_in_schema=False)
async def token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    user = userSchema.UserLogin(email=form_data.username, password=form_data.password)
    loginData = await authRoutes.login(user, db)
    return {'access_token': loginData['user'].auth['token'], 'token_type':  'bearer'}

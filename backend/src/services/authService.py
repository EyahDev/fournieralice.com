import toml
from fastapi import Depends, HTTPException, Header, status
from fastapi_mail import FastMail
from fastapi.security import OAuth2PasswordBearer
from fastapi.security.utils import get_authorization_scheme_param
from sqlalchemy.orm import Session
from jose import JWTError, jwt
from passlib.context import CryptContext
from typing import Optional
from datetime import datetime, timedelta
from pydantic import ValidationError
from yattag import Doc

#local import
from schemas.userSchema import UserToken
from api import userApi
from database import get_db

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
jwt_config = toml.load('./config/security.toml')['jwtConfig']

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)

def hash_password(password):
    return pwd_context.hash(password)

def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=60)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, jwt_config.get('secret_key'), algorithm=jwt_config.get('algorithm'))
    return encoded_jwt


def authenticate_user(db, username: str, password: str):
    user = userApi.get_user_by_email(db, username)
    if not user or not verify_password(password, user.password):
        return False
    del user.password
    return user


def get_email_from_token(token: str) -> UserToken:
    token_content_exception = HTTPException(
        status_code = 400,
        detail = "Le contenu du token est invalide",
        headers = {"WWW-Authenticate": "Bearer"},
    )

    credentials_exception = HTTPException(
        status_code = status.HTTP_401_UNAUTHORIZED,
        detail = "Le token utilisateur a expiré",
        headers = {"WWW-Authenticate": "Bearer"},
    )

    try:
        payload = jwt.decode(
            token, jwt_config.get('secret_key'),
            algorithms = [jwt_config.get('algorithm')]
        )

        try:
            token_data = UserToken(email=payload['sub'])
            return token_data
        except ValidationError:
            raise token_content_exception
    except JWTError:
        raise credentials_exception


def get_token_from_header(*, authorization: str = Header(None)) -> UserToken:
    token_type_exception = HTTPException(
        status_code = status.HTTP_401_UNAUTHORIZED,
        detail = "Le type de token attendu est invalide",
        headers = {"WWW-Authenticate": "Bearer"},
    )

    scheme, token = get_authorization_scheme_param(authorization)

    if scheme.lower() != "bearer":
        raise token_type_exception

    token_data = get_email_from_token(token)
    return token_data


def get_forgotten_pwd_email_content(reset_pwd_token: str):
    mailForgottenPwd = toml.load('./config/security.toml')['mailForgottenPwd']
    reinitUrl = mailForgottenPwd.get('reinitUrl') + reset_pwd_token

    doc, tag, text = Doc().tagtext()
    with tag('html'):
        with tag('body'):
            with tag('p'):
                text(mailForgottenPwd.get('headContent'))
            with tag('a', href=reinitUrl):
                text(mailForgottenPwd.get('urlName'))
            with tag('p'):
                text(mailForgottenPwd.get('footContent'))
    return doc.getvalue()


async def send_token_email(user: UserToken):
    reset_pwd_token_expires = timedelta(minutes=jwt_config.get('expire_forgot_pwd'))
    reset_pwd_token = create_access_token(
        data={"sub": user.email}, expires_delta=reset_pwd_token_expires
    )

    mailForgottenPwd = toml.load('./config/security.toml')['mailForgottenPwd']

    content = get_forgotten_pwd_email_content(reset_pwd_token)

    mailer = toml.load('./config/config.toml')['mailer']
    mail = FastMail(
        email = mailer.get('sender'),
        password = mailer.get('password'),
        tls = mailer.get('tls'),
        port = mailer.get('port'),
        service = mailer.get('server')
    )
    await mail.send_message(recipient=user.email,subject=mailForgottenPwd.get('title'), body=content, text_format="html")
    return True

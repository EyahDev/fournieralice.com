import shutil
from fastapi import FastAPI, UploadFile
from typing import List

def write_uploaded_file(path, filename, uploaded_file: UploadFile):
    with open(path + filename, "wb") as buffer:
        shutil.copyfileobj(uploaded_file.file, buffer)
        return True
    return False

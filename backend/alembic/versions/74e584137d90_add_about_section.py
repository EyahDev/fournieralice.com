"""add about section

Revision ID: 74e584137d90
Revises: 90cd7a45967b
Create Date: 2020-12-06 14:22:29.786689

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '74e584137d90'
down_revision = '90cd7a45967b'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('users',
        sa.Column('about', sa.UnicodeText(), nullable=True)
    )


def downgrade():
    op.drop_column('users', 'about')

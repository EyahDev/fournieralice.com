"""create news table

Revision ID: 073243dbec5d
Revises: 74e584137d90
Create Date: 2020-12-06 16:07:47.518218

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '073243dbec5d'
down_revision = '74e584137d90'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'news',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('title', sa.String(255), nullable=False),
        sa.Column('content', sa.UnicodeText(), nullable=False),
        sa.Column('date', sa.DateTime(timezone=True), nullable=False, server_default=sa.sql.func.now())
    )


def downgrade():
    op.drop_table('news')

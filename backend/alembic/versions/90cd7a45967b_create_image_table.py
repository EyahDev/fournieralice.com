"""create image table

Revision ID: 90cd7a45967b
Revises: 6e058d7fc097
Create Date: 2020-11-29 12:20:46.595325

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '90cd7a45967b'
down_revision = '6e058d7fc097'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'images',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('path', sa.String(255), nullable=False),
        sa.Column('filename', sa.String(255), nullable=False),
        sa.Column('description', sa.String(255))
    )


def downgrade():
    op.drop_table('images')

"""create user table

Revision ID: 6e058d7fc097
Revises:
Create Date: 2020-07-08 00:29:34.765287

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6e058d7fc097'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('email', sa.String(255), nullable=False),
        sa.Column('password', sa.String(255)),
        sa.Column('firstname', sa.String(255)),
        sa.Column('lastname', sa.String(255)),
        sa.Column('phone', sa.String(255)),
    )


def downgrade():
    op.drop_table('users')

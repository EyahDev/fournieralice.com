import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { BackofficeRoutingModule } from './components/backoffice/backoffice-routing.module';
import { NotFoundComponent } from './components/commons/not-found/not-found.component';

const routes: Routes = [
  { path: '', component: AppComponent},
  { path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    BackofficeRoutingModule
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }

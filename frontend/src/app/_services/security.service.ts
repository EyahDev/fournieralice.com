import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { MessagesService } from './messages.service';
import { UserService } from './user.service';

declare const UIkit: any;


@Injectable({
  providedIn: 'root',
})
export class SecurityService {

  /**
   * @param userService - Service de gestion de l'utilisateur connecté
   * @param http - Client http pour la consommation des API
   * @param messagesService - Le service de messages pour l'insertion de message entre les pages
   */
  constructor(private userService: UserService, private http: HttpClient, private messagesService: MessagesService) { }

  /**
   * Process d'authentification
   *
   * @param authValues - identifiants de connexion
   * @return any
   */
  authProcess(authValues): any {
    return this.http.post('/auth/login', authValues).toPromise().then((httpResponse: any) => {
        this.messagesService.clearMessage('error', 'login');

        this.userService.setUser(httpResponse.user, authValues.loginRememberMe);
        this.userService.setUserOnStorage(authValues.loginRememberMe, this.userService.getUser());
    }).catch((httpErrorResponse: any) => {
      this.messagesService.setErrorMessage('login', httpErrorResponse.error.detail);
    });
  }

  /**
   * Process de modification du mot de passe de l'utilisateur
   *
   * @param passwordsValues - les valeurs du formulaire du changement de mot de passe
   */
  changePasswordProcess(passwordsValues): any {
    const user = this.userService.getUser();
    const headers = this.userService.getAuthorizationHeaders();

    const values = {
      email: user.email,
      password: passwordsValues.oldPassword,
      new_password: passwordsValues.newPassword,
      confirmation: passwordsValues.confirmNewPassword,
    };

    return this.http.post('/user/password', values, { headers })
      .toPromise().then((httpResponse: any) => {
      this.messagesService.clearMessage('error', 'changePassword');

      UIkit.notification({
        message: 'Votre mot de passe a été mis à jour.',
        pos: 'top-right',
        status: 'success'});

    }).catch((httpErrorResponse: any) => {
      this.messagesService.setErrorMessage('changePassword', httpErrorResponse.error.detail);
    });
  }

  /**
   * Process du mot de passe oublié
   *
   * @param email - Email du mot de passe oublié
   * @return any - Retourne un message d'erreur ou de succès
   */
  forgottenPasswordProcess(email): any {
    return this.http.post('/auth/forgotten-password', { email: email })
      .toPromise().then((httpResponse: any) => {
        this.messagesService.clearMessage('error', 'forgottenPassword');
        this.messagesService.setSuccessMessage('forgottenPassword', httpResponse.message);
      }).catch((httpErrorResponse: any) => {
        this.messagesService.setErrorMessage('forgottenPassword', httpErrorResponse.error.detail);
      });
  }

  /**
   * Vérification si le token de réinitialisation de mot de passe est valide ou non
   *
   * @param token - token d'authentification
   */
  resetPasswordTokenIsValid(token): boolean {
    // TODO Appel à l'API pour la vérification de la validité du token
    return true;
  }

  /**
   * Process de réinitialisation du mot de passe
   *
   * @param token - Token d'authentification
   * @param newPassword - Nouveau mot de passe
   */
  resetPasswordProcess(token, newPassword): any {
    return this.http.post('/auth/reinit-password', { password: newPassword, token: token })
      .toPromise().then((httpResponse: any) => {
        this.messagesService.clearMessage('error', 'reinitPassword');
        this.messagesService.setSuccessMessage('reinitPassword', httpResponse.message);
      }).catch((httpErrorResponse: any) => {
        this.messagesService.setErrorMessage('reinitPassword', httpErrorResponse.error.detail);
      });

    /*const isResetted = true;

    if (!isResetted) {
      return {
        type: 'error',
        content: 'Une erreur s\'est produite, veuillez réessayer.'
      };
    }

    return {
      type: 'success',
      content: 'Votre mot de passe a été réinitialisé.'
    };*/
  }

  /**
   * Process de déconnexion de l'utilisateur de l'administration
   */
  logoutProcess(): void {
    this.userService.logoutUser();
  }
}

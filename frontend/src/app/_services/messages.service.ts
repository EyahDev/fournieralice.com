import { Injectable } from '@angular/core';

@Injectable()
export class MessagesService {
  messages: any = {
    error: [],
    warning: [],
    success: [],
    info: [],
  };

  /**
   * Récupération de tous les messages
   *
   * @return string
   */
  getAllMessages(): string {
    return this.messages;
  }

  /**
   * Récupération d'un message en fonction de son type et de son id
   *
   * @param type - le type de message
   * @param messageId - l'id du message
   */
  getMessage(type: string, messageId: string): object|undefined {
    return this.messages[type].find(i => i.id === messageId);
  }

  /**
   * Set un message d'erreur à afficher
   *
   * @param messageId - identifiant du message
   * @param message - Message à enregistrer
   */
  setErrorMessage(messageId: string, message: string): void {
    this.messages.error.push({ id: messageId, content: message });
  }

  /**
   * Récupération de tous les messages erreurs
   *
   * @return array - La liste des messages de type error
   */
  getErrorMessages(): string {
    return this.messages.error;
  }

  /**
   * Set un message de de succès à afficher
   *
   * @param messageId - identifiant du message
   * @param message - Message à enregistrer
   */
  setSuccessMessage(messageId: string, message: string): void {
    this.messages.success.push({ id: messageId, content: message });
  }

  /**
   * Récupération de tous les messages succès
   *
   * @return array - La liste des messages de type success
   */
  getSuccessMessages(): string {
    return this.messages.success;
  }

  /**
   * Set un message de warning à afficher
   *
   * @param messageId - identifiant du message
   * @param message - Message à enregistrer
   */
  setWarningMessage(messageId: string, message: string): void {
    this.messages.warning.push({ id: messageId, content: message });
  }

  /**
   * Récupération de tous les messages warning
   *
   * @return array - La liste des messages de type warning
   */
  getWarningMessages(): string {
    return this.messages.warning;
  }


  /**
   * Set un message d'info à afficher
   *
   * @param messageId - identifiant du message
   * @param message - Message à enregistrer
   */
  setInfoMessage(messageId: string, message: string): void {
    this.messages.info.push({ id: messageId, content: message });
  }

  /**
   * Récupération de tous les messages info
   *
   * @return array - La liste des messages de type info
   */
  getInfoMessages(): string {
    return this.messages.infos;
  }

  /**
   * Supprime un message grace à son id et son type de message
   *
   * @param type - Type de message
   * @param messageId - L'identifiant du message à supprimer
   */
  clearMessage(type, messageId): void {
    const message = this.getMessage(type, messageId);
    const messages = this.messages[type];

    if (message) {
      messages.splice(messages.indexOf, 1);
    }
  }

  /**
   * Supprime tous les messages stockés
   */
  clearAll(): void {
    this.messages.error = [];
    this.messages.warning = [];
    this.messages.success = [];
    this.messages.info = [];
  }
}

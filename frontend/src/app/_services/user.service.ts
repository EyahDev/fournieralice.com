import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as moment from 'moment';

import { MessagesService } from './messages.service';

declare const UIkit: any;

@Injectable()
export class UserService {
  userLoggedIn: any;

  /**
   * @param http - Client http pour la consommation des API
   * @param messageService - Le service de gestion des messages de l'application
   */
  constructor(private http: HttpClient, private messageService: MessagesService) {
    this.userLoggedIn = this.getUser();
  }

  /**
   * Ajoute l'utilisateur sessionStorage et en localStorage dans le cas ou il est nécessaire de sauvegarder l'utilisateur pendant 7 jours
   *
   * @param remember - Se souvenir de l'utilisateur pendant 7 jours ou non
   * @param user - L'utilisateur à sauver
   */
  setUserOnStorage(remember: boolean, user: any): void {
    if (remember || localStorage.getItem('fournieralice.user')) {
      localStorage.setItem('fournieralice.user', JSON.stringify(user));
    } else {
      sessionStorage.setItem('fournieralice.user', JSON.stringify(user));
    }
  }

  /**
   * Récupération des headers http pour l'authentification aux API sécurisées
   *
   * @return HttpHeaders - Les headers nécessaires à l'authentification
   */
  getAuthorizationHeaders(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `${this.userLoggedIn.auth.token_type} ${this.userLoggedIn.auth.token}`
    });
  }

  /**
   * Récupération de l'utilisateur couramment connecté
   *
   * @return any - l'utilisateur couramment connecté
   */
  getUser(): any {
    if (this.userLoggedIn) {
      return this.userLoggedIn;
    }

    const LSUser = JSON.parse(localStorage.getItem('fournieralice.user'));
    const SSUser = JSON.parse(sessionStorage.getItem('fournieralice.user'));

    return LSUser ? LSUser : SSUser;
  }

  /**
   * Vérification si l'utilisateur est toujours valide (7 jours sous remember me et 24h sans)
   *
   * @return boolean - retourne l'utilisateur, false si non
   */
  userValidCheckProcess(): boolean {
    const user = this.getUser();

    if (!user) {
      return false;
    }

    let userIsValid = true;

    // Vérification de la validité de l'utilisateur si remember me (15 jours)
    if (user.remember && moment().diff(moment(user.latestLoginDate, 'LL LTS').add(15, 'd')) > 0) {
      localStorage.removeItem('fournieralice.user');
      userIsValid = false;
    }

    // Vérification de la validité de l'utilisateur hors remember me (24 heures)
    if (!user.remember && moment().diff(moment(user.latestLoginDate, 'LL LTS').add(24, 'h')) > 0) {
      sessionStorage.removeItem('fournieralice.user');
      userIsValid = false;
    }

    if (!userIsValid) {
      this.userLoggedIn = null;

      this.messageService.setErrorMessage('userValidity', 'Pour des raison de sécurité, veuillez vous reconnecter.');
    }

    return userIsValid;
  }

  /**
   * Supprime l'utilisateur du service lors de la déconnexion
   */
  logoutUser(): void {
    sessionStorage.removeItem('fournieralice.user');
    localStorage.removeItem('fournieralice.user');

    this.userLoggedIn = null;
  }

  /**
   * Set un utilisateur comme étant l'utilisateur couramment connecté
   *
   * @param user - Utilisateur à set
   * @param rememberUser - Se souvenir de l'utilisateur
   */
  setUser(user: any, rememberUser: boolean|null): void {
    const latestLoginDate = this.userLoggedIn ? this.userLoggedIn.latestLoginDate : moment().format('LL LTS');
    const auth = this.userLoggedIn ? this.userLoggedIn.auth  : user.auth;
    const remember = this.userLoggedIn ? this.userLoggedIn.rememberMe : rememberUser;

    this.userLoggedIn = {
      latestLoginDate,
      auth,
      remember,
      id: user.id,
      firstname: user.firstname,
      lastname: user.lastname,
      email: user.email,
      phone: user.phone
    };
  }

  /**
   * Process du changement des informations de l'utilisateur
   *
   * @param userInfosValues - Les nouvelles informations de l'utilisateur
   */
  updateInfosProcess(userInfosValues): any {
    const headers = this.getAuthorizationHeaders();

    const user = {
      id: this.userLoggedIn.id,
      firstname: userInfosValues.firstname,
      lastname: userInfosValues.lastname,
      phone: userInfosValues.phone,
      email: userInfosValues.email
    };

    this.http.post('/user/informations',
      user, { headers }).toPromise().then((httpResponse: any) => {
      this.setUser(httpResponse, null);
      this.setUserOnStorage(false, this.userLoggedIn);

      UIkit.notification({
        message: 'Vos informations ont été mises à jour.',
        pos: 'top-right',
        status: 'success'});
    }).catch((httpErrorResponse: any) => {
      console.error(httpErrorResponse.error.detail);
      UIkit.notification({
        message: 'Une erreur s\'est produite, veuillez réessayer.',
        pos: 'top-right',
        status: 'danger'});
    });
  }
}

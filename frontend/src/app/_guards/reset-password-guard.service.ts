import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';

import { Observable } from 'rxjs';
import { SecurityService } from '../_services/security.service';
import { UserService } from '../_services/user.service';

@Injectable()
export class ResetPasswordGuardService implements CanActivate {
  constructor(private securityService: SecurityService, private userService: UserService,
              private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this.userService.userLoggedIn && this.securityService.resetPasswordTokenIsValid(route.params.token)) {
      return true;
    }

    return this.router.navigate(['/administration']);
  }

}

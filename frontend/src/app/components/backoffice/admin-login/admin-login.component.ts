import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import { LoginBaseComponent } from '../../commons/base/login-base/login-base.component';
import { UserService, SecurityService, MessagesService } from '../../../_services';

@Component({
  selector: 'app-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.less']
})
export class AdminLoginComponent extends LoginBaseComponent implements OnInit {
  loginForm: FormGroup;
  loginFormIsSubmitted = false;

  constructor(
    private titleService: Title,
    private securityService: SecurityService,
    private messageService: MessagesService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    super(userService);
  }

  ngOnInit(): void {
    this.titleService.setTitle('fournieralice.com - Administration');

    this.loginForm = this.formBuilder.group({
      email: [null, [Validators.required, Validators.email]],
      password: [null, Validators.required],
      loginRememberMe: [false]
    });

    if (this.userService.getUser()) {
      this.router.navigate(['administration/dashboard']);
    }
  }

  onSubmitLoginForm(): void {
    this.loginFormIsSubmitted = true;

    if (this.loginForm.valid) {
      this.securityService.authProcess(this.loginForm.value).finally(() => {
        const errorMessage = this.messageService.getMessage('error', 'login');

        if (!errorMessage) {
          this.router.navigate(['administration/dashboard']);
        }
      });
    }
  }
}

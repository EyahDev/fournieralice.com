import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { SecurityService, UserService } from '../../../_services/';
import { MustMatch } from '../../../_validators/must-match.validator';

@Component({
  selector: 'app-admin-user-info',
  templateUrl: './admin-user-settings.component.html',
  styleUrls: ['./admin-user-settings.component.less']
})
export class AdminUserSettingsComponent implements OnInit {
  userInfosFormIsSubmitted = false;
  userPasswordFormIsSubmitted = false;
  userAboutFormIsSubmitted = false;

  userInfosForm: FormGroup;
  userNewPasswordForm: FormGroup;
  userAboutForm: FormGroup;

  tinyMCEConfig: any;

  userLoggedIn: any;
  message: any;

  constructor(private userService: UserService, private formBuilder: FormBuilder, private securityService: SecurityService) {
  }

  ngOnInit(): void {
    this.tinyMCEConfig = {
      base_url: '/tinymce', // Root for resources
      suffix: '.min',
      language: 'fr_FR',
      language_url: 'assets/tinymce-fr_FR.js',
      height: 350,
      menubar: false,
      plugins: [
        'advlist autolink lists link image charmap print preview anchor',
        'searchreplace visualblocks code fullscreen',
        'insertdatetime media table paste code help wordcount'
      ],
      toolbar:
        'undo redo | preview | bold italic underline strikethrough | removeformat | \
        alignleft aligncenter alignright alignjustify | \
        bullist numlist | formatselect fontsizeselect  | help'
    };

    this.userLoggedIn = this.userService.getUser();

    // Formulaire pour le changement des informations de l'utilisateur
    this.userInfosForm = this.formBuilder.group({
      firstname: [this.userLoggedIn.firstname, Validators.required],
      lastname: [this.userLoggedIn.lastname, Validators.required],
      email: [this.userLoggedIn.email, [Validators.required, Validators.email]],
      phone: [this.userLoggedIn.phone, [Validators.required, Validators.pattern('^(?:(?:\\+|00)33|0)\\s*[1-9](?:[\\s.-]*\\d{2}){4}$')]],
    });

    // Formulaire pour le changement du mot de passe de l'utilisateur
    this.userNewPasswordForm = this.formBuilder.group({
      oldPassword: [null, Validators.required],
      newPassword: [null, [Validators.required, Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$')]],
      confirmNewPassword: [null, Validators.required]
    }, {
      validator: MustMatch('newPassword', 'confirmNewPassword')
    });

    // Formulaire pour le changement du mot de passe de l'utilisateur
    this.userAboutForm = this.formBuilder.group({
      about: ['Je suis une valeur initial de tinyMCE'],
    });
  }

  onSubmitUserInfosForm(): void {
    this.userInfosFormIsSubmitted = true;

    if (this.userInfosForm.valid) {
      this.userService.updateInfosProcess(this.userInfosForm.value);
    }
  }

  onSubmitUserNewPasswordForm(): void {
    this.userPasswordFormIsSubmitted = true;

    if (this.userNewPasswordForm.valid) {
      this.securityService.changePasswordProcess(this.userNewPasswordForm.value).finally(() => {
        this.userNewPasswordForm.reset();
      });
    }
  }

  onSubmitUserAboutForm(): void {
    this.userAboutFormIsSubmitted = true;

    if (this.userAboutForm.valid) {
      console.log(this.userAboutForm.value);
    }
  }
}

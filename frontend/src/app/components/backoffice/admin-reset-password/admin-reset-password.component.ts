import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { LoginBaseComponent } from '../../commons/base/login-base/login-base.component';
import { MustMatch } from '../../../_validators/must-match.validator';
import { SecurityService, UserService } from '../../../_services/';

@Component({
  selector: 'app-reset-password',
  templateUrl: './admin-reset-password.component.html',
  styleUrls: ['./admin-reset-password.component.less']
})
export class AdminResetPasswordComponent extends LoginBaseComponent implements OnInit {
  resetPasswordForm: FormGroup;
  resetPasswordFormIsSubmitted = false;

  message: any;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private securityService: SecurityService,
              private userService: UserService)
  {
    super(userService);
  }

  ngOnInit(): void {
    this.resetPasswordForm = this.formBuilder.group({
      firstPassword: [null, [Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$')]],
      secondPassword: [null, Validators.required]
    },  {
      validator: MustMatch('firstPassword', 'secondPassword')
    });
  }

  onSubmitResetForm(): void {
    this.resetPasswordFormIsSubmitted = true;

    if (this.resetPasswordForm.valid) {
      this.message = this.securityService.resetPasswordProcess(
        this.activatedRoute.snapshot.params.token, this.resetPasswordForm.value.firstPassword);

      if (this.message.type !== 'error') {
        this.router.navigate(['/administration']);
      }
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminForgottenPasswordComponent } from './admin-forgotten-password.component';

describe('ForgottenPasswordComponent', () => {
  let component: AdminForgottenPasswordComponent;
  let fixture: ComponentFixture<AdminForgottenPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminForgottenPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminForgottenPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

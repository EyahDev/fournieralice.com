import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService, SecurityService } from '../../../_services/';
import { LoginBaseComponent } from '../../commons/base/login-base/login-base.component';

@Component({
  selector: 'app-forgotten-password',
  templateUrl: './admin-forgotten-password.component.html',
  styleUrls: ['./admin-forgotten-password.component.less']
})
export class AdminForgottenPasswordComponent extends LoginBaseComponent implements OnInit {
  forgottenPasswordForm: FormGroup;
  forgottenPasswordFormIsSubmitted = false;

  message: any;

  constructor(private formBuilder: FormBuilder, private securityService: SecurityService, private userService: UserService) {
    super(userService);
  }

  ngOnInit(): void {
    this.forgottenPasswordForm = this.formBuilder.group({
      forgottenPasswordEmail: [null, [Validators.required, Validators.email]],
    });
  }

  onSubmitForgottenPasswordForm(): void {
    this.forgottenPasswordFormIsSubmitted = true;

    if (this .forgottenPasswordForm.valid) {
      this.message = this.securityService.forgottenPasswordProcess(this.forgottenPasswordForm.value.forgottenPasswordEmail);
    }
  }
}

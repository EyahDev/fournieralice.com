import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {SecurityService} from '../../../_services/security.service';

@Component({
  selector: 'app-admin-navbar',
  templateUrl: './admin-navbar.component.html',
  styleUrls: ['./admin-navbar.component.less']
})
export class AdminNavbarComponent implements OnInit {
  constructor(private securityService: SecurityService, private route: Router) { }

  ngOnInit(): void {
  }

  onLogout(): void {
    this.securityService.logoutProcess();
    this.route.navigate(['/administration']);
  }

}

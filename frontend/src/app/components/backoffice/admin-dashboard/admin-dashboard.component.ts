import { Component, OnInit } from '@angular/core';

import { LoginBaseComponent } from '../../commons/base/login-base/login-base.component';
import { UserService } from '../../../_services/';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.less']
})
export class AdminDashboardComponent extends LoginBaseComponent implements OnInit {
  userLoggedIn;

  constructor(private userService: UserService) {
    super(userService);
  }

  ngOnInit(): void {
    this.userLoggedIn = this.userService.getUser();
  }

}

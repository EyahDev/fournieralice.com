import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from '../../_guards/auth-guard.service';
import { ResetPasswordGuardService } from '../../_guards/reset-password-guard.service';

import { AdminUserSettingsComponent } from './admin-user-settings/admin-user-settings.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminForgottenPasswordComponent } from './admin-forgotten-password/admin-forgotten-password.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
import { AdminResetPasswordComponent } from './admin-reset-password/admin-reset-password.component';

const routes: Routes = [
  { path: 'password/forgotten', component: AdminForgottenPasswordComponent},
  { path: 'password/reset/:token', component: AdminResetPasswordComponent, canActivate: [ResetPasswordGuardService]},
  { path: 'administration', component: AdminLoginComponent},
  { path: 'administration/dashboard', component: AdminDashboardComponent, canActivate: [AuthGuardService]},
  { path: 'administration/user-settings', component: AdminUserSettingsComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class BackofficeRoutingModule { }

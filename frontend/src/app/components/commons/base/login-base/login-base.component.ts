import { Component, Input, OnInit } from '@angular/core';

import { UserService } from '../../../../_services';
import { tsParticles } from 'tsparticles';

@Component({
  selector: 'app-login-base',
  templateUrl: './login-base.component.html',
  styleUrls: ['./login-base.component.less']
})
export class LoginBaseComponent implements OnInit {
  @Input() component: any;

  constructor(private uService: UserService) {
    tsParticles.loadJSON('particles', 'assets/particles-config.json');
  }

  ngOnInit(): void { }
}

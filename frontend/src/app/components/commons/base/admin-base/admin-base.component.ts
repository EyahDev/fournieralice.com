import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin-base',
  templateUrl: './admin-base.component.html',
  styleUrls: ['./admin-base.component.less']
})
export class AdminBaseComponent implements OnInit {
  @Input() component: any;

  constructor() { }

  ngOnInit(): void {
  }

}

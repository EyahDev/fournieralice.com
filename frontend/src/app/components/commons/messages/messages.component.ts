import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../../../_services';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.less']
})
export class MessagesComponent implements OnInit {
  success: any;
  warnings: any;
  errors: any;
  infos: any;

  constructor(private messageService: MessagesService) { }

  ngOnInit(): void {
    this.success = this.messageService.getSuccessMessages();
    this.warnings = this.messageService.getWarningMessages();
    this.errors = this.messageService.getErrorMessages();
    this.infos = this.messageService.getInfoMessages();
  }

}

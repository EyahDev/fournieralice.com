import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { EditorModule } from '@tinymce/tinymce-angular';

// Import des routes et du composants App
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Import des services et des guards
import { UserService, MessagesService, SecurityService } from './_services/';
import { AuthGuardService } from './_guards/auth-guard.service';
import { ResetPasswordGuardService } from './_guards/reset-password-guard.service';
import { ApiInterceptor } from './_interceptors/api.interceptor';

// Import des composants
import { NotFoundComponent } from './components/commons/not-found/not-found.component';
import { AdminDashboardComponent } from './components/backoffice/admin-dashboard/admin-dashboard.component';
import { AdminNavbarComponent } from './components/backoffice/admin-navbar/admin-navbar.component';
import { AdminUserSettingsComponent } from './components/backoffice/admin-user-settings/admin-user-settings.component';
import { AdminForgottenPasswordComponent } from './components/backoffice/admin-forgotten-password/admin-forgotten-password.component';
import { AdminLoginComponent } from './components/backoffice/admin-login/admin-login.component';
import { AdminResetPasswordComponent } from './components/backoffice/admin-reset-password/admin-reset-password.component';
import { AdminBaseComponent } from './components/commons/base/admin-base/admin-base.component';
import { LoginBaseComponent } from './components/commons/base/login-base/login-base.component';
import { MessagesComponent } from './components/commons/messages/messages.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    LoginBaseComponent,
    AdminBaseComponent,
    AdminLoginComponent,
    AdminDashboardComponent,
    AdminForgottenPasswordComponent,
    AdminResetPasswordComponent,
    AdminNavbarComponent,
    AdminUserSettingsComponent,
    MessagesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    EditorModule
  ],
  providers: [
    MessagesService,
    UserService,
    SecurityService,
    AuthGuardService,
    ResetPasswordGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
